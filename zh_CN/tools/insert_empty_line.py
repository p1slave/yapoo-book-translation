import sys

print(sys.argv)

for filename in sys.argv[1:]:
	with open(filename, 'r') as f:
		lines = f.readlines()
		print('Reading file: %s' % filename)
	with open(filename, 'w') as f:
		for line in lines:
			f.write(line)
			if line.strip() != '':
				f.write('\n')