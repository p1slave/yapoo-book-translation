# 家畜人鸦俘（第八章 前往二千年后的地球）

### 1 圆筒船｢冰河号｣
一个多小时前二名中学生正在滕达山山脚下采蝶，突然目击一圆盘状物体坠落在山中，受少年旺盛的好奇心驱使他们立刻转向山路往山中走去，就在这个时候两人不由得停下脚步，注视着前方的天空｡原来，突然间一个巨大的圆柱物体彷佛凭空出现似地浮现在空中｡直径三十公尺圆形平底，高一百公尺的完美圆柱状，日本人大概会联想到茶叶筒吧!光滑无窗散发着橙橘色光芒的物体离地垂直，静止不动｡

｢啊，大圆柱!｣

｢和厄隆那次一样｡刚才那个果然是 UFO!｣

一九五二年十月，南法厄隆市圆盘与圆筒伴随出现，事后还留下｢天使头发｣之称的奇特蒸发纤维后离去，没有一个对｢幽浮｣感兴趣的人不知道那个UFO史上曾经发生过的事实｡二名少年兴高采烈，恨不得快点抵达现场｡直觉圆柱体静止不动的下方想必是刚才目击的圆盘型UFO｡ 未知的物体､世界之谜､里面一定有外星人……不知不觉他们拔跑飞奔了起来｡

山路沿着溪流蜿蜓而转，二名少年终于来到有间小木屋的山腰空地一角｡

｢啊!小木屋……｣

｢ UFO!｣

小木屋被压垮了，上面坐着一架犹若末完工般部分毁损的圆盘型UFO｡ 而圆柱型UFO悠悠静止在正上方离地约三百公尺的上空｡

话说飞碟内的驾驶舱——

突然响起一串铃声｡宝琳切下开关，立体显影机出现德莉丝的半身像，她对其姐说:

｢好像故障了，上不来吗?｣

｢嗯，无法运转｡用牵引线吊上去吧!｣

｢知道了｡我去准备蓝光放射线，稍等一会儿｡｣

匆忙交谈后影像消失｡宝琳看着克莱儿道:

｢等会儿空气会变蓝，别担心｡｣

话一说完没多久整个舱内便被蓝色气体包围，站在窗边的感觉好像黄昏情境，同时出现搭乘电梯时的上升感，桌上的立体雷达显示的｢冰河号｣船体正在快速接近扩大中｡

｢对了，克莱儿小姐，我该如何把你介绍给大家呢?｣

宝琳略带担忧地说:｢他们要是知道你是前史时代的人，一定不肯让你搭乘｡如果能够慢慢地解释并说服他们也可以，但却无法使他们立刻同意，因为这样做是违法的!所以我想了一个点子｡你就说你也是邑司人，跟我一样在时光旅行中失控坠毁或是你任性地擅自主张着陆而降落在这个球面上｡不过又因某种因素造成时光机故障而无法回去，加上严重撞击使你丧失过去的记忆｡除了记得名字是克莱儿外，姓氏､出生星球､经历甚至连邑司人的事都忘得一干二净｡事故发生前，你捕获的鸦俘忠实地守在身边侍奉你照顾你｡然后，我坠落时被你所救｡我一见你就认定你是邑司人，你看见飞盘的时候，朦胧中似乎也想起自己是出生在邑司的某殖民星球上｡所以我决定带你回来，如何?我编的故事还可以吧?｣

｢好吧｡如果不这么说就不能搭乘的话，只好出此下策了｡｣

｢对你来说，不曾见识过的事物多如牛毛，丧失记忆的说法应该可以暂时蒙混过去｡就快收进舱底，他们马上就到了｡｣

紧接着蓝光消失，飞碟瞬间静止｡

跑到飞碟近旁的一名少年因为突然间深蓝色光圈的笼罩，伸手不见五指，忍不住尖叫出声｡

随后跑来的少年看见眼前突然冒出一道纯蓝色光柱时悚然一惊，随即停下脚步｡

那道巨柱来自离地三百公尺上空的圆柱体整个底部｡光束与地面垂直放射，完全罩住飞碟｡大约一分半钟，光柱消失｡教人惊讶的是飞碟也不见了｡站在光柱中的那名少年指着上空说:

｢它升上去了!周围全是蓝光，伸手不见五指，但我的手摸到了 U F O的边缘｡它在向上移动……｣

抬头一看，圆柱型UFO的圆底有一同心圆的黑圈正急速地缩小，很快就化成一个黑点消失无踪｡

｢该不会跟照相机的构造一样吧?｣

｢嗯，开启洞门把UFO收回去又关上｡｣

｢那道蓝光是什么东西啊?｣

｢为的是不让我们目击UFO升空吧!｣

｢天啊，消失了!｣

再没有比消失更贴切的形容词了｡因为圆柱体UFO就这样在两人抬头观看的视线内凭空消失｡白云飘浮的晴空教两人感到格外地眩目刺眼｡

二名少年愣然地站立着，宛若蜘蛛丝般轻柔的蓝线飘拂在他们脸上｡环顾四周，那种奇妙的东西还有几根在周围飘散｡

｢真的是天使头发!｣

｢那是太空船啊!｣

｢好想一睹外星人的真面目噢!｣

兴奋地交谈着的两人并末注意到旁边有一条脚步匆促的灵猖犬穿越离去｡

后来他们再三指证那艘圆柱体UFO忽然消失一事时，大多数的人们部认为两名少年把超音速飞行夸张化了｡他们深感不满｡对于圆柱体是时空船､天使发是蓝光线空间牵引线的残迹等他们全然不知｡他们却直觉认为圆盘与圆柱都不属于这个世界｡警方根据他们的报告进行现场搜索，结果发现两匹马的尸体与麟一郎的衣物，太空船的说法摇身一变成了苏联的秘密武器，至于二名日德男女离奇失踪一事则报导成疑似逃亡或被绑架至东德或苏联，最后只有这二名少年深信自己真的看见了太空船｡其实也不能说他们不正确｡虽非他们想像中的外星人，但UFO中的那些人确实不属于二十世纪｡

### 2 三贵族登场
室温调节功能可能也故障了吧?上升时舱内突然变得很冷｡突然间门被人从外打开，一名美少女飞奔进来｡原来是德莉丝｡她的个子比姐姐高，大约有一百八十公分以上｡头上戴着的马球帽藏不住那头浓密的金发，帽檐下是一对深蓝得彷佛黑湖般的杏眼｡说她非常美丽，脸部线条却又稍嫌刚硬而且表情精悍，但嫣红的唇十分可爱｡宽格条纹的马球上衣掩去了她隆起的胸部｡跟一个小时前克莱儿在显像机中看到的是同一件衣服｡可见她无暇换下马球运动装就坐上｢冰河号｣了｡先前看不到的下半身，修长的双腿套着一身的骑马裤，脚踏一双长靴，右靴的靴筒插着一条鞭子，鞭柄露在外面｡皮革泛着漂亮的淡红色光泽，克莱儿不知那是洗靴奴以泪洗靴的天马皮革，还拿来跟自己刚脱下的马靴比较，觉得自己那双马靴有如乞丐鞋般破烂寒酸｡更甭论衣料了｡幸好听了她的劝把衣服换了｡如果穿着那种衣服､鞋子岂不羞死人了｡

克莱儿感到年轻女性应有的放心与满足｡

接着又走进两个人｡克莱儿无法分辨对方是男是女｡就穿着打扮来看似乎是女人｡德莉丝没修指甲，可那两人的指甲上却擦着色彩鲜艳的指甲油｡其中一人身穿类似和服全衣的长礼服，长及地的裙上绣着花朵图案，映入趴在地上的麟一郎视线一角，几乎错以为走来的是身穿和服的日本妇女｡另一人虽着长裤却色彩鲜艳，至于上半身穿的衣服，若以二十世纪人一般的观点视之应该是女人穿的衬衫，感觉上非常华丽，比起克莱儿惜穿的轻便套装更像女人穿的｡前者的金发编成辫子垂在脸庞两侧，后者将浓密的亚麻色头发往后梳扎成一把，头上戴着一顶无沿小帽，丰姿绰约｡尽管服装与发型都疑似妇女，面容与身躯却是十足的男性｡穿和服的那位比克莱儿矮一点，穿长裤的那位看似一百八十五公分其实穿着高跟鞋，所以显得高大｡从他们身体的线条和藏在衣袍下的强壮肌肉，分明是男性无异｡长裤的股间也明显地鼓起｡

两人相貌堂堂｡金发的那位，一看就知道是宝琳与德莉利丝的手足｡纤浓的眉毛，长长的眼睫，线条优美的鼻梁，樱桃般的淡粉红唇，肌层如玉贝又似乳白色的琥珀，而且没有胡髭，不过却是一张男人的脸｡而亚麻色头发的那位，若以美男子论之也许略逊前者，但轮廓线条很有个性，充满了年轻的朝气｡他有两道英气逼人的浓眉，大而明亮的灰眸，鹰勾鼻，受阳光眷顾的麦色肌肤，也算得上是一名美男子｡前者举手投足之间一如衣饰非常地女性化，后者却不同｡但淡然微笑着的柔和神情看在克莱儿眼底，女性气质还是多过了男性｡两人的年纪都在二十几岁左右，后者比较年轻｡

话说回来，肉体上倒没有不分男女或半男半女般变态，如果能够理解奇特的男性服饰是基于不同的风俗习惯就不会觉得错乱了｡他们的举止如此优雅斯文，躯体到底还是男性｡说起男人，克莱儿从未遇过两人都旗鼓相当､浑身充满魅力的男子｡尤其是那名年轻男子比前者少了一点阴柔气质，就连身为二十世纪女子的克莱儿也深受吸引｡德国籍的克莱儿身材高挑，对自己过高的身材一直怀有自卑感 ，所以宝琳和德莉丝也同样高挑一事让她觉得很安心｡

前来的男女分别戴着戒指，戒指上镶着漂亮的宝石｡只是二名男子都戴着手环，其中一人甚至戴着耳坠，女性则末佩戴｡是马球服的关系吧?三人相继与宝琳拥抱､亲吻并献上祝福，每一回都有不一样的芳香袭向克莱儿｡究竟是每个人都用不同的香水?还是各有特殊的体味?但她万万不能露出不可思议的表情，因为她现在扮演的是一名邑司人……

宝琳逐一地介绍三人｡

｢克莱儿小姐，让我为你引见｡这是我妹德莉丝·琼森，十几岁的小姑娘噢｡是个运动迷，马球队的正式选手，计划在下一届的奥运中拿下五项竞赛(相当于二十世纪奥运的近代五项竞赛)金牌而努力练习中｡旁边这位是家兄赛西尔，已婚，目前是梅儿莉·得莱帕伯爵(女性和男性的称谓和曾经的相反)忠实的妻君，专攻家畜文化史的学者｡古语也很厉害｡他的主人得莱帕勋爵是国军中坚干部(狄铎乔尼肯宇宙帝国中央军司令官､地球都督､大将军､公爵的通称｡后章将再详述)的参谋，而且被推举为星际战争竞赛(各惑星相互对抗，令鸦俘军队战斗厮杀，比较战术优劣的竞赛)的卡尔星球代表军｡得莱帕伯爵是素有名望的古老家族｡而那位是得莱帕勋爵之弟，威廉·得莱帕少爷，虽是一介男子却热爱挥刀舞剑的快意生活，是厄伯敦赫赫有名的野男儿噢｡他具备黄金虫大赛的A级证照｡这位是克莱儿小姐，在我危难之际是她出手相救｡至于姓氏我们待会儿再谈｡在威廉名下加上少爷称谓，是针对已婚男子的妻君以为区别的未婚男子姓名之冠称，也是女权制度确立后，男子童贞受到重视后所发展出来的新名词｡称呼女性的小姐､女士依然沿用旧有的惯称，而男性的少爷､妻君则相当于二十世纪的小姐､夫人的称谓，若撇开爵位不谈，梅儿莉·得莱帕小姐与赛西尔·琼森少爷结婚后便通称为梅儿莉·得莱帕夫人妻君｡男方赛西尔必须冠上得莱帕的女方姓氏｡夫人､妻君的称谓就等于以前的先生跟太太｡

克莱儿与他们一一握手｡宝琳说道:

｢这位小姐的遭遇相当奇特｡因此至今连自己的姓氏､出生星球都毫无记忆｡｣

｢什么?她丧失记忆?｣

三人异口同声地惊呼｡

宝琳把事情的来龙去脉简要地说明一番｡从坠落事故､克莱儿搭救､发现带着鸦俘的她是邑司人，到知道她丧失了过去的记忆､纽曼又咬伤了鸦俘……一如先前套好的故事情节｡

当宝琳谈起克莱儿不认为这只土着鸦俘是鸦俘而把它当人类一样看待时，三人惊讶不已，坦白直率的德莉丝哑然失笑，却被其姐责备｡

｢总之她失去了邑司人的生活记忆，过着原始的生活，所以我打算帮她恢复记忆｡｣

宝琳大声说完后，便用克莱儿听不见的低语与三人商议｡

｢记忆恢复前的她和前史时代人没有两样｡当她发现飞碟并且走进舱内后，眼睛看到，耳朵听到，好像就记起一些了｡如果她的反应有点奇怪，绝不可以嘲笑她噢｡因为她也一心地想找回失去的记忆，

反正她不是平民，一定是殖民星球的贵族｡｣

三人一致同意，决定尽己所能帮助克莱儿恢复记忆｡他们不会对她投以冒失无礼的目光，反倒对裸身趴在室内一角､服侍过流浪女主人的土着鸦俘，不曾收敛他们好奇的视线｡

克莱儿感到不快｡她不喜欢心爱的未婚夫被亳不避讳的目光上下打量｡假设他们注视麟一郎的视线让她觉得带有敌意或否定的评价，也许她会心生反感｡然而他们的目光并未如此｡甚至连轻蔑的意味也没有｡他们表现出来的只是纯粹的好奇心｡他们看待麟一郎就像自己看见一匹新马时所流露的目光｡那教克莱儿渐感不安｡她突然想起刚才宝琳脱口而出的日语｡难道我错了?麟真的是那种似人而非人的鸦俘?这个疑问开始在克莱儿的心中萌芽｡

### 3 腕式话器与颅内受话器
｢距离目的地还有一个小时｡我们到楼上的船舱吧!｣

相互介绍完毕后，德莉丝说｡

｢我不能离开麟，我是说这只鸦俘，健康的话无所谓，可他现在全身麻痹……｣

这回她不能说濑部先生了｡既然假装丧失记忆，意图冒充邑司人，迫不得已只好称麟一郎为鸦俘，不过克莱儿并未忘记这趟旅行的目的是解除他的麻痹症状｡虽然麟一郎是不是鸦俘的怀疑心开始在克莱儿的心中萌芽，但距丢下麻痹痛苦的他不管而自私地接受邀宴的心境，还有一段时间｡或许怜悯的成分多过爱情，可是克莱儿从没想过要离弃他｡

……然而大家并不了解｡

｢瞧你，就算你再重视它，也不必对鸦俘讲情义讲到这种地步嘛，要我们把你这个贵客扔在这里，自顾自地逍遥自在，也说不过去啊!｣德莉丝说｡

｢你在不在身边都一样噢｡注射之前它不会有任何变化的，而注射又要等到抵达目的地之后……｣威廉也说｡

｢才一个小时嘛!要是不放心，我让别人帮你看着，毕竟不在二十世纪的球面了……｣

宝琳差点脱口而出:再这样犹豫不决，就不像邑司人了!

面对他们轮番劝说如何救助麟一郎比较高明，克莱儿渐渐被说动了｡旧时代人的身份被识破一切就前功尽弃｡正当克莱儿不知所措之际，俊美得连女人都心动的赛西尔红着脸嗫嚅地说道:

｢恕我直言……你的鸦俘必须进行导尿处置噢｡士着鸦俘跟我们人类一样具有泌尿排泄系统，麻痹症状会使它无法排泄，备受折磨｡以前曾经发生过捕获的旧石器时代人在归途的时空船中因膀胱破裂而死的事件，从那之后，因攻击牙而被麻醉的鸦俘通常都会插入导尿管｡更何况任着鸦俘赤身露体下去将使它感染肺炎，所以有一些处置是势在必行的｡

幸亏有人注意到了，直趴蹲在旁､听觉神经紧张的麟一郎暗自庆幸着｡室温在进入时空船舱后似乎正急速下降，从刚才就略感寒意｡随着一波波袭来的寒意，膀胱的饱服感也逐渐涌现｡

已经更衣解手过的克莱儿感受不到寒意与尿意｣因此并未特别留心，所幸赛西尔是这方面的专家，对土着鸦俘的生理状况非常了解｡

｢赛西尔说的没错｡｣宝琳立刻出言附和，其实她都忘了，连忙抢话解释｡｢我本想处理却碍于游艇上没有设备也没有黑奴……｣

｢这艘船内应该两者皆有……｣

｢我立刻交代黑奴｡｣德莉丝做出结论｡

｢克莱儿小姐，你还是别旁观的好，黑奴比较习惯面对那种场面……｣

对方的意思是鸦俘的排泄作业会污染人类的眼与手，最好予以谢绝，可惜克莱儿没有会意过来｡不过思及自己先前的动作，想必麟一郎也有生理上的需要｡至于目击插入导尿管的作业，使克莱儿踌躇了起来｡她与德莉丝的想法相反，克莱儿承认麟一郎的人格，认为他是一个昂扬男子，亲睹那种场面会使她感到羞涩｡若是熟悉彼此身体的夫妇也许无所谓，但两人始终保持纯净的精神恋爱……赤身体会感染肺炎这句话，听在她的耳里直觉认为对方有意为他添加衣物｡如果克莱儿知道那句话代表某种意思的话，说不定她会采取别的行动｡不过对鸦俘本就全裸的事实一概不知的克莱儿，无法理解话中真意也无可厚非｡她用那种送病人住院时的心情请求他们:

｢既然如此，就万事拜托了｡我愿意离开现场｡麻烦你们带路｡｣事到如今，也无法严词责备听从对方建议的克莱儿｡

德莉丝按下左手腕上类似手表的表把，盖子打开后凑近唇嘴，低声交代着｡

｢我们在船库的飞碟驾驶舱内，客人带着土着鸦俘同行，它被攻击牙咬伤，尼安德塔人用的棺器有吧?立刻从猎物置场派二名过来｡｣

这是腕式送话器，使役黑奴之用的间接小型指示机｡活体家具发达的今日，贵族依然在家庭里畜养黑奴｡因为上流阶级有很多地方必须维持一如既往的生活型态，所以光凭活体家具是不够的｡例如进餐时要求侍候的仆役站在后方听令差遣是贵族家庭的习惯之一，这类侍候的仆役并非鸦俘而是黑奴｡他们是从白人家庭内使唤的仆役中择选出最优秀的黑奴，称为侍者或随从，构成黑奴的最高阶层｡

黑奴是半人类，不同于鸦俘，人权多少受到承认，因此黑奴体内不能安装读心装置(能够安装的高O Q的黑奴也很少见)｡对他们传达命令必须利用超短波放送｡所有侍者都经脑外科手术，在耳廓内的外耳道下方安装受话器，接收固定的周波数｡当命令传到指示机也就是烟盒般大小的送话器时将形成一个迷你广播站，立刻化成电波送至侍者耳中再还原成音波，于是耳内回响的就是主人的命令｡虽然须以某特定的受信人为对象放送，但被使唤的那方没有拒绝受信的自由，即便沉眠的时候，也无法阻止耳内充塞的命令｡这就是颅内受话器(此物并非仅用来接受主人的命令｡比方说邑司贵族都有健康防御装置，就是指以短波接收主人心跳并整书整夜倾听放大功率的从畜)｡不过要主人们一一记住各侍者的周波数或

每次下令就要重新设定，实在麻烦，因此通常会给侍长一台直接指示机，自己则利用间接指示机命令侍长｡间接指示机以侍长们耳内受话器为对象，只需数人的周波数即可，构造十分迷你｡这就是腕式送话器｡各侍长必须透过脑波追踪装置时列掌握旗下十几名侍者的目前所在位置，接收命令后，再选派并监督适合的侍者完成工作｡又名中继奴，一般职场单位部有一组，｢冰河号｣有好十几名黑奴所以分成三组｡德莉丝搭乘的同时就戴上了该船专用的腕式送话器｡

｢克莱儿小姐，你尽管放心啦!走吧，大家去上面的大厅｡｣

德莉丝率先迈步前行｡宝琳催促似地劝克莱儿一起走，克莱儿谦让地说:｢我随后就来｡｡｡｡｡｡｣

宝琳､人犬纽曼､赛西尔､威廉等人依序前行｡克莱儿打算留到最后，安慰并鼓励麟一郎几句后再离开，不料得莱帕少爷停下了脚步，微笑地伸出手来等她回应;待克莱儿返身走近舱内一角时看见赤身露体四脚伏地的鳞一郎，似乎察觉到自己为了这名泥巴色的黄皮肤男子竟让阿波罗般俊美的青年久候｡于是不发一语地转身向右，挽着阿波罗般的手一同离开｡

把趴在地上的麟一郎孤独地留下，与肉足凳､舌人形为伴｡

——克莱儿别走!别抛下我一个!

他在心中拚命地叫喊，奈何唇动也不动，教他焦急难耐……

舱内愈来愈冷，尿意也愈来愈急｡